# Manisha

Manisha is a Nagios (and compatible) [monitoring
plug-in](https://www.monitoring-plugins.org/) to test Gemini
servers. It allows to test automatically if the servers work fine from
monitoring schedulers like Nagios or [Icinga](https://icinga.com/). It
can test the status code, the content of the answer, etc.

[Gemini](https://gemini.circumlunar.space/) is a lightweight system to
distribute information (an alternative to the Web).

Manisha is written in Python.

## Usage

Manisha follows the usual Nagios rules. The options are:

* -H: host name or address to monitor
* -V: virtual hostname (necessary for the Gemini protocole, and the certificate check will be based on that)
* -p: path in the URL (do not add a leading slash)
* -e: a string to expect in the answer
* -c: the status code to expect (20, by default)
* -E: a regular expression to expect in the answer
* -C: the expiration date of the certificate (as days_before_warning,days_before_critical)
* -m: a string to expect in the meta-answer (for instance `text/gemini`)
* -M: a regular expression to expect in the meta-answer 
* -P: port to use (the default is 1965)
* -g: get the content of the resource (only way, with Gemini, to get the size)
* -i: insecure (do not check the certificate)
* -T: disable TOFU certificate check
* -a: accept expired certificates
* -x: do not send the SNI
* -4: force a test over IPv4
* -6: force a test over IPv6
* -s: goes through a SOCKS proxy, expressed as "host:port"

For Icinga, the following definition enables the plugin:

```
object CheckCommand "gemini" {
  command = [ PluginContribDir + "/check_gemini" ]

  arguments = {
     "-H" = {{
	   var v6only = macro("$gemini_forceipv6$")
	   var v4only  = macro("$dot_forceipv4$")
	   var addr_v4 = macro("$address$")
	   var addr_v6 = macro("$address6$")
           if ((addr_v6 && !v4only) || v6only) {
		return addr_v6
	   } else {
		return addr_v4
	   }
	   }},
      "-V" = "$gemini_vhost$",
      "-e" = "$gemini_expect$",
      "-E" = "$gemini_regexp_expect$",
      "-c" = "$gemini_expect_statuscode$",
      "-C" = "$gemini_certificate$",
      "-m" = "$gemini_expect_meta$",
      "-M" = "$gemini_regexp_expect_meta$",
      "-p" = "$gemini_path$",
      "-P" = "$gemini_port$",
      "-s" = "$gemini_proxy$",	    
      "-i" = { set_if =	"$gemini_insecure$" },
      "-T" = { set_if =	"$gemini_no_tofu$" },
      "-a" = { set_if =	"$gemini_accept_expired$" },
      "-x" = { set_if = "$gemini_nosni$" },
      "-g" = { set_if = "$gemini_get_content$" },
      "-4" = "$gemini_force_ipv4$",
      "-6" = "$gemini_force_ipv6$"
  }

}

```

And a possible use is:

```
apply Service for (gemini_vhost => config in host.vars.gemini_vhosts) {
  import "generic-service"

  check_command = "gemini"
  assign where (host.address || host.address6) && host.vars.gemini
  vars += config
}

```

```
object Host "radia" {
...
  vars.gemini_vhosts["vhost1"] = {
      gemini_expect = "This is virtual host 1"
      gemini_vhost = "vhost1.example"
   }

  vars.gemini_vhosts["vhost2"] = {
      gemini_expect = "This is virtual host 2"
      gemini_vhost = "vhost2.example"
   }


```

## Installation

You need Python 3 and [Agunua](https://framagit.org/bortzmeyer/agunua). You can install Agunua, for instance, 
with pip `pip3 install agunua`.

Then, copy the script `check_gemini.py` to the directory of local
plugins.

## Name

Manisha is the girlfriend of Gemini in [a movie](https://en.wikipedia.org/wiki/Gemini_(2002_film)).

## License

GPL. See LICENSE.

## Authors

Stéphane Bortzmeyer <stephane+framagit@bortzmeyer.org>.

## Reference site

https://framagit.org/bortzmeyer/manisha/ Use the Gitlab issue tracker to
report bugs or wishes. But you can of course also access it with
gemini at gemini://gemini.bortzmeyer.org/software/manisha/.

